import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MainColl {

    public static void main(String[] args) {
        System.out.println("Hello world!");

        //Po lewej - interfejs; po prawej - konkretna klasa
        List<String> list = new ArrayList<>(); //alt+ctrl+b - lista implementacji
        list.add("Kasia");
        list.add("Basia");
        list.add("Zosia");

        List<String> names = Arrays.asList("Wojtek", "Tomek", "Gosia", "Kasia");


        list.addAll(names); //dodaje do kolekcji list kolekcję names

        System.out.println(list); //wyświetli całą listę

        list.remove("Kasia"); //usunie pierwszą Kasię z listy
        System.out.println(list);

        Collections.sort(list); //posortowana lista (sposób domyślny, sortowanie naturalne)

        System.out.println(list);

        Collections.sort(list, Collections.reverseOrder());//odwrotna kolejność sortowania

        List integers = Arrays.asList(1,7,3,5,4,9,65,21);

        System.out.println(integers);

        Collections.sort(integers); //posortowana lista intów - domyślnie

        System.out.println(integers);

    }

}
