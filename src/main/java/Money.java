public class Money {

    private int value;

    public Money (int value){
        this.value = value;
    }

    @Override
    public String toString() {
        return "Money{" +
                "value=" + value +
                '}';
    }

    public static void main(String[] args) {
        Money a = new Money(100);
        Money b = new Money(100);

        System.out.println(a==b); //dwie różne referencje, bo stworzyliśmy 2 obiekty
        System.out.println(a.equals(b)); //metoda z klasy Object
    }

    @Override
    public boolean equals(Object o) {
        //porównanie referencji
        if (this == o) return true;
        //spr, czy przekazany obiekt jest null i czy należy do tej samej klasy
        if (o == null || getClass() != o.getClass()) return false;

        Money money = (Money) o;

        return value == money.value;
    }

    @Override
    public int hashCode() {
        return value;
    }
}


